import 'package:flutter/material.dart';

void main() {
  runApp(
      MyApps()
  );
}

class MyApps extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return
      MaterialApp(
          home: Scaffold(
            backgroundColor: Colors.blue,
            appBar: AppBar(
              title: Text('Melajah Flutter Sendiri'),
              backgroundColor: Colors.amber,
            ),
              body: PictureApps()
          )
      );
  }
}

class PictureApps extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Image(
          image: NetworkImage('https://undiksha.ac.id/wp-content/uploads/2016/10/new-LOGO.png'),
        ),
      );
  }
}



